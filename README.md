package supermarket1;

import java.util.Objects;

public class Product {
	private int productID;
	public String productName;
	public float productPrice;
	private String productionDate;
	private String expiryDate;
	
	public Product(int productID,String productName,float productPrice,String productionDate,String expiryDate) {
		this.productID=productID;
		this.productName=productName;
		this.productPrice=productPrice;
		this.productionDate=productionDate;
		this.expiryDate=expiryDate;
	}
	
	

	public String getProductName() 
	{
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public float getProductPrice()
	{
		return productPrice;
	}
	public void setProductPrice(float productPrice)
	{
		this.productPrice=productPrice;
	}
	
	public String getProductionDate()
	{
		return productionDate;
	}
	public void setProductionDate(String productionDate)
	{
		this.productionDate=productionDate;
	}
	
	public String getExpiryDate()
	{
		return expiryDate;
	}
	public void setExpiryDate(String expiryDate)
	{
		this.expiryDate=expiryDate;
	}
	
	public int getProductID()
	{
		return productID;
	}
	
	@Override
	public int hashCode() {
		int hash=7;
		hash=29*hash + Objects.hashCode(this.productID);
		hash=29*hash + Objects.hashCode(this.productName);
		hash=29*hash + Objects.hashCode(this.productPrice);
		hash=29*hash + Objects.hashCode(this.productionDate);
		hash=29*hash + Objects.hashCode(this.expiryDate);
		return hash;
	}
	
	@Override
	public boolean equals(Object obj)
	{
		 if (this == obj) {
	            return true;
	        }
	        if (obj == null) {
	        	return false;
	        }
	        if (getClass() != obj.getClass())
	        {
	        	return false;
	        }
	        final Product other=(Product) obj;
	        if(!Objects.equals(this.productName, other.productName))
	        {
	        	return false;
	        }
	        if(!Objects.equals(this.productID,other.productID))
	        {
	        	return false;
	        }
	        if(!Objects.equals(this.productPrice, other.productPrice))
	        {
	        	return false;
	        }
	        if(!Objects.equals(this.productionDate, other.productionDate))
	        {
	        	return false;
	        }
	        if(!Objects.equals(this.expiryDate,other.expiryDate))
	        {
	        	return false;
	        }
		
	        
	}
	public void setProductID(int productID)
	{
		this.productID=productID;
	}

}
